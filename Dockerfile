FROM php:8.2-apache
RUN apt-get update && \
    apt-get install -y \
        libzip-dev \
        zip \
        unzip \
        && docker-php-ext-install zip pdo pdo_mysql mysqli
WORKDIR /var/www/html
COPY . /var/www/html
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer update
RUN chown -R www-data:www-data /var/www/html/storage
COPY .env.example .env
RUN sed -i 's/DB_HOST=.*/DB_HOST=172.17.0.1/' /var/www/html/.env && \
    sed -i 's/DB_PORT=.*/DB_PORT=3306/' /var/www/html/.env && \
    sed -i 's/DB_DATABASE.*/DB_DATABASE=point-of-sales/' /var/www/html/.env && \
    sed -i 's/DB_USERNAME.*/DB_USERNAME=alfian/' /var/www/html/.env && \
    sed -i 's/DB_PASSWORD.*/DB_PASSWORD=tirta/' /var/www/html/.env
RUN php artisan key:generate
RUN php artisan migrate
RUN php artisan db:seed
COPY laravel.conf /etc/apache2/sites-available
RUN a2dissite 000-default.conf && a2enmod rewrite && a2ensite laravel.conf
RUN service apache2 restart
EXPOSE 80
